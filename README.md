### check_logfile

Intended to demonstrate TDD and GitLab-CI.
Goals include :-

   1. File existence/readability
   2. File Size
   3. History / Alarm Duration
   4. Global and Local Pattern Matching definitions, as well as pre-probe variables
   5. Efficiency - using seek()/tell()/stat() to improve performance
   6. Capacity Planning - alarms based on file size and standard deviation
   7. Enabling/Disabling reset of "handled" status upon threshold breach
   
Work in progress, as requested by JAC / SVE